<?php
require_once "src/config.php";

class DB
{
    public static $dbase;

    const DB_NAME_QUESTIONS = 'questions';
    const DB_NAME_TESTS = 'tests';

    public function __construct()
    {
        global $config;

        //singletone
        if(self::$dbase == null){
            self::$dbase = new mysqli($config['db']['host'], $config['db']['user'], $config['db']['password'], $config['db']['name']);
        }

        if(!mysqli_connect_errno()){
            $sql = "SELECT count(1) as c FROM ".self::DB_NAME_QUESTIONS;
            $query = self::$dbase->query($sql);

            if ($query === false) {
                throw new Exception(self::$dbase->error, self::$dbase->errno);
            }

            // extract the value
            $row = $query->fetch_assoc();
            if(isset($row['c']) && $row['c'] == 0){
                $sql = "";
                for($i = 1; $i <= 100; $i++){
                    $sql .= "INSERT INTO ".self::DB_NAME_QUESTIONS." (title) VALUES('".self::$dbase->escape_string("Вопрос $i")."');";
                }

                if (!self::$dbase->multi_query($sql)) {
                    echo "Не удалось выполнить мультизапрос: (" . self::$dbase->errno . ") " . self::$dbase->error;
                }


                do {
                    if ($res = self::$dbase->store_result()) {
                        var_dump($res->fetch_all(MYSQLI_ASSOC));
                        $res->free();
                    }
                } while (self::$dbase->more_results() && self::$dbase->next_result());


            }
        }else{
            die("Настройте соединение с базой Mysql в конфиге src/config.php и залейте руками dump.sql");

        }

        return DB::$dbase;



    }

    function __destruct()
    {
        self::$dbase->close();
    }

    public function getHistory()
    {
        $sql = "SELECT *  FROM ".self::DB_NAME_QUESTIONS;
        $query = self::$dbase->query($sql);

        return $query->fetch_all();

    }

    public function upCount($ids)
    {
        $sql = "UPDATE ".self::DB_NAME_QUESTIONS." SET history_count=history_count+1 WHERE id IN(".implode(",", $ids).")";
        return self::$dbase->query($sql);
    }

    public function upTests($ids)
    {
        for($i = 1; $i <= 100; $i++){
            $sql .= "INSERT INTO ".self::DB_NAME_TESTS." (title) VALUES('".self::$dbase->escape_string("Вопрос $i")."');";
        }

        if (!self::$dbase->multi_query($sql)) {
            echo "Не удалось выполнить мультизапрос: (" . self::$dbase->errno . ") " . self::$dbase->error;
        }



    }


}
<?php

class Controller
{
    private $db;
    public function __construct()
    {
        $this->db = new DB();
    }

    public function Main()
    {
        echo file_get_contents("Views/Main.html");

    }

    public function History()
    {
        $res = $this->db->getHistory();
        $table = "";

        foreach ($res as $question) {
            $table .= "<tr>";
            $table .= "<td>{$question[0]}</td>";
            $table .= "<td>{$question[1]}</td>";
            $table .= "<td>{$question[2]}</td>";
            $table .= "</tr>";
        }

        $html =  file_get_contents("Views/History.html");

        $repArr = array(
            "%table%" => $table,
            "%intellect%" => UrlRouter::$queryParams['intellect'],
            "%com_from%" => UrlRouter::$queryParams['com_from'],
            "%com_to%" => UrlRouter::$queryParams['com_to']
        );

        $html = strtr($html, $repArr);

        echo $html;

    }

    public function RunTest()
    {
        $questions = $this->db->getHistory();

        $qCounts = [];
        foreach ($questions as $question){
            $qCounts[] = $question[2];
        }

//        $max = max($qCounts);

//        if(!$max){
//            $max = 100;
//        }
        //не идеально далено, но на большее не было времени
        $iQ = [];
        for($i = 1; $i <= 100; $i++){
           $iQ[] = mt_rand(0, 100);
        }
        shuffle($questions);

        $testQuest = [];
        $qCounts = 0;
        foreach ($questions as $key => $quest){

            if($iQ[$key] > $quest[2]){
                $testQuest[] = $quest;
                $qCounts++;
            }

            if($qCounts == 40) break;
        }


        $qIds = [];
        $countWin = 0;
        $intellect = UrlRouter::$queryParams['intellect'];
        $comFrom = UrlRouter::$queryParams['com_from'];
        $comTo = UrlRouter::$queryParams['com_to'];



        foreach ($testQuest as $key => $quest){
            $qIds[] = $quest[0];

            $intel = mt_rand(0, $intellect);
            $quest = mt_rand($comFrom, $comTo);

            //можно было подсветить в таблице, времени не хватило
            if($intel > $quest){
                $testQuest[$key]['res'] = 1;
                $countWin++;
            }else{
                $testQuest[$key]['res'] = 0;
            }
        }

        $this->db->upCount($qIds);

        echo $countWin;
    }

}
<?php
require_once 'Controllers/DB.php';
require_once 'Controllers/class.Controller.php';

class UrlRouter
{
    private $req;
    private $rPatterns;

    static public $queryParams;

    public function __construct()
    {
        global $config;

        $this->req = $_SERVER['QUERY_STRING'];
        $this->rPatterns = $config['req'];
        self::$queryParams = $_REQUEST;
    }

    public function run()
    {
        $contClass = new Controller;
        foreach ($this->rPatterns as $pattern => $controller){
            if(preg_match($pattern, $this->req)){
                $contClass->$controller();
                break;
            }
        }

    }

}
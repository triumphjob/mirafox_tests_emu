<?php

global $conf;

$config = array(
    db => array(
        'host'  => '127.0.0.1',
        'user' => 'root',
        'password' => 'q1w2e3r4t5y6',
        'name' => 'mirafox_tests'
    ),

    'req' => array(
        '/^p=2$/' => 'History',
        '/^p=ajax/' => 'RunTest',
        '/^$/' => 'Main',
        '/.*/' => 'Main'

    )
);
CREATE DATABASE mirafox_tests CHARACTER SET utf8 COLLATE utf8_general_ci;


create table questions
(
  id            int auto_increment
    primary key,
  title         tinytext      null,
  history_count int default 0 not null
)
  comment 'List of questions';

create table tests
(
  id              int auto_increment
    primary key,
  question_id     int     null,
  complexity_from tinyint null,
  complexity_to   tinyint null,
  constraint tests_questions_id_fk
    foreign key (question_id) references questions (id)
)
  comment 'Consists tests information';

create table users_tests
(
  id           int auto_increment
    primary key,
  intellect_to tinyint null
)
  comment 'Users for new tests';

create table tests_history
(
  id          int auto_increment
    primary key,
  user_id     int     null,
  question_id int     null,
  result      tinyint null,
  constraint tests_history_questions_id_fk
    foreign key (question_id) references questions (id),
  constraint tests_history_users_tests_id_fk
    foreign key (user_id) references users_tests (id)
)
  comment 'Consist tests history';


